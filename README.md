# Teleperformance

## Instalar dependencias
Una vez descargado el proyecto del repositorio se debe acceder al directorio llamado teleperformance/ donde se encuentra el frontend desarrollado con Angular. Ubicado en la carpeta se deben instalar las dependencias para ello se ejecuta el siguiente comando:
```
npm install
```

## Iniciar servidor
Para iniciar el servidor ejecutar el siguiente comando:
```
ng serve --port 4210
```
En el navegador de preferencia ingresar el siguiente enlace para ver el proyecto:
```
http://localhost:4210/
```

## Nota:
Es importante tener en cuenta que el enlace para consumir el API es la siguiente (en ese enlace corre el API desarrollado con Express js):
```
http://localhost:7001/api
```
Como se puede observar en el archivo environment.ts que se encuentra en la siguiente ubicación:
```
teleperformance/src/environments/environment.ts

```

# Instrucciones de la Aplicación:
En el "home" se puede observar un listado de los personajes de la serie Rick and Morty, dicha información es consumida desde un API pública por medio del backend desarrollado en Express js.

La aplicación cuenta con una serie de formularios:
```
1. Login: Permite acceder al perfil del usuario con su respectivo correo electrónico y contraseña.
2. Registro: Permite a un usuario registrarse en la aplicación ingresando nombre, correo electrónico, contraseña y rol.
3. Actualizar perfil: Permite a un usuario que haya iniciado sesión actualizar su información personal.
``` 

IMPORTANTE: Para acceder al formulario de login se debe hacer por medio del menú que se encuentra en la parte superior derecha.

## Para usuario administrador
Estas son las acciones que puede realizar un usuario de tipo administrador dentro de su perfil:
```
1. Actualizar su información: Nombre, correo electrónico y contraseña.
2. Ver en una tabla el listado de usuarios registrados en la aplicación.
```

## Para usuario normal
Estas son las acciones que puede realizar un usuario normal dentro de su perfil:
```
1. Actualizar su información: Nombre, correo electrónico y contraseña.
2. Ver en una tabla el listado de personajes de la serie Rick and Morty.
```

Es necesario destacar que:
```
1. Un usuario de tipo administrador no puede acceder a la ruta del perfil de un usuario normal y viceversa, debido a que la aplicación "protege" y permite acceder a los enlaces de acuerdo al tipo de usuario.
2. Se implemento el localStorage del navegador para almacenar/controla la información de la sesión del usuario.
3. La aplicación es responsive.
4. La base de datos se encuentra en MongoDB Atlas que es un servicio alojado en la nube de AWS.
```

# Tecnologías:
## Implementadas en la aplicación:
```
1. Angular 12 (JS)
2. Angular Material
3. Bootstrap
```

## Implementadas en el control de versiones:
```
1. Git
2. Bitbucket (repositorio)
```