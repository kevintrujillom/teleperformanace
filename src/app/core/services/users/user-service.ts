import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ResponseApi } from '../../interfaces/users/response-api.interface';
import { Rol } from '../../interfaces/users/rol-interface';
import { User } from '../../interfaces/users/user-interface';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  getUser(): Observable<User> {
    return this.http.get<User>(`${environment.api}`)
      .pipe(
        map(response => response, (error: any) => error)
      );
  }

  getUsers(): Observable<ResponseApi> {
    return this.http.get<ResponseApi>(`${environment.api}users/`)
      .pipe(
        map(response => response, (error: any) => error)
      );
  }
  
  getRols(): Observable<Rol[]> {
    return this.http.get<Rol[]>(`${environment.app}assets/lista-roles.json`);    
  }

  register(user: Partial<User>): Observable<ResponseApi> {
    return this.http.post<ResponseApi>(`${environment.api}users`, user)
      .pipe(
        map( response => response, (error: any) => error)
      );
  }  
  
  update(id: string, user: Partial<User>): Observable<ResponseApi> {
    return this.http.put<ResponseApi>(`${environment.api}users/${id}`, user).pipe(
        map(
          (response) => response,
          (error: any) => error
        )
      );      
  }
}
