import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ResponseApi } from '../../interfaces/characters/response-api.interface';

@Injectable({
  providedIn: 'root'
})
export class CharactersService {

  constructor(
    private http: HttpClient,
  ) { }

  getCharacters(): Observable<ResponseApi> {
    return this.http.get<ResponseApi>(`${environment.apiRest}`)
      .pipe(
        map(response => response, (error: any) => error)
      );
  } 

}
