import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ResponseApi } from '../interfaces/users/response-api.interface';
import { User } from '../interfaces/users/user-interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  private logoutSubject = new Subject<boolean>();
  private logintSubject = new Subject<boolean>();
  
  logoutObservable = this.logoutSubject.asObservable();
  loginObservable = this.logintSubject.asObservable();  
    
  constructor(
    private http: HttpClient,
    private router: Router
  ) {}

  login(user: Partial<User>): Observable<ResponseApi> {
    return this.http.post<ResponseApi>(`${environment.api}/auth/login`, user).pipe(
      map(
        (response) => response,
        (error: any) => error
      )
    );
  }

  loginObser(isLogin: boolean): void {
    this.logintSubject.next(isLogin);
  }

  redirect(user: User): void {
    const id = user.rol;
    if (id === 1) {
      this.router.navigate(['perfil/admin']);
    } else if (id === 2) {
      this.router.navigate(['perfil/usuario']);
    }
  }  

  logout(isLogout: boolean): void {
    this.logoutSubject.next(isLogout);
  }

}
