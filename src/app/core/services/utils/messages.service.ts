import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  constructor(
    private toastr: ToastrService
  ) { }

  showMessage(header: string, success: boolean, message: string = ''): void {
    if (success) {
      this.toastr.success(message, header);
    } else {
      this.toastr.error(message, header);
    }
  }  
}