export interface User {
    _id: string;
    name: string;
    email: string;
    password: string;
    rol: number;
    createdAt?: string;
    updatedAt?: string;
}