import { Content } from "./content.interface";

export interface ResponseApi{
    exist?: boolean;
    success: boolean;
    content: Content;
}
