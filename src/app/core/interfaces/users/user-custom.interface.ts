export interface UserCustom {
    name: string;
    email: string;
    rol: number;
}