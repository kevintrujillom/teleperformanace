export interface CharacterCustom {
    gender: string;
    id: number;
    name: string;
    species: string;
}