import { Origin } from "./origin.interface";

export interface Character {
    created?: string;
    episode?: any[];
    gender?: string;
    id?: number
    image?: string;
    location?: Origin;
    name?: string;
    origin?: Origin
    species?: string;
    status?: string;
    type?: string;
    url?: string;    
}
