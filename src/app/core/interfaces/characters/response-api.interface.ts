import { Character } from "./character.interface";
import { Information } from "./information-iinterface";

export interface ResponseApi {
    info?: Information;
    results?: Character[];
}