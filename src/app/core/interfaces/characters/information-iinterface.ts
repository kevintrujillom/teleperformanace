    export interface Information {
        count: number;
        next: string;
        pages: number;
        prev: string
    }
    