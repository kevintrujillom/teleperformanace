import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { User } from '../interfaces/users/user-interface';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {

    user!: User;

    constructor( 
      private router: Router,
      private authService: AuthService
    ) { }

    canActivate(): boolean {

        this.user = JSON.parse(localStorage.getItem('user_teleperformance')!);

        if (this.user) {

        if (this.user.rol === 2) {
    
            return true;
    
        } else {
    
            this.authService.redirect(this.user);
            return false;
        }

        } else {
        
            this.router.navigate(['login']);
            return false;      
        }

    }  
  
}