import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cutString'
})
export class CutStringPipe implements PipeTransform {

  transform(value: string | undefined): any {
    if (value!.length > 18) {

      return value!.substr(0, 18) + '...';

    } else {
      
      return value!;
    }
}

}
