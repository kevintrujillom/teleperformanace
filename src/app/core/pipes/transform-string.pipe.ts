import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'transformString'
})
export class TransformStringPipe implements PipeTransform {

  transform(value: number): any {
    if (value === 1) {

      return 'Administrador';

    } else if (value === 2) {
      
      return 'Usuario';
    }
  }

}
