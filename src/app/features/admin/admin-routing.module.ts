import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from 'src/app/core/guards/admin.guard';
import { UserGuard } from 'src/app/core/guards/user.guard';
import { AdminComponent } from './pages/admin/admin.component';
import { UserComponent } from './pages/user/user.component';

const routes: Routes = [
  {
    path: 'admin', canActivate: [AdminGuard], component: AdminComponent
  },
  {
    path: 'usuario', canActivate: [UserGuard], component: UserComponent
  }     
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
