import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UserCustom } from 'src/app/core/interfaces/users/user-custom.interface';
import { User } from 'src/app/core/interfaces/users/user-interface';
import { UserService } from 'src/app/core/services/users/user-service';
import { MessagesService } from 'src/app/core/services/utils/messages.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  formProfile!: FormGroup;
  emailPattern: any = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  
  authUser!: User;
  users: User[] = [];
  usersTable: UserCustom[] = [];
  esVisible = false;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private messagesService: MessagesService,
    private toastr: ToastrService    
  ) { 
    this.createForm();
  }

  ngOnInit(): void {
    this.getUser();
    this.getUsers();    
  }

  createForm(){
    this.formProfile = this.formBuilder.group({
      name: [''],
      email: ['', [Validators.pattern(this.emailPattern)]],
      password: ['']
    });
  }

  getUser(): void {
    this.authUser = JSON.parse(localStorage.getItem('user_teleperformance')!);
    this.formProfile.get('name')!.setValue(this.authUser.name);
    this.formProfile.get('email')!.setValue(this.authUser.email);
  }

  update(): void {
    const UserUpdate = {
      name: this.formProfile.get('name')?.value ? this.formProfile.get('name')?.value : this.authUser.name,
      email: this.formProfile.get('email')?.value ? this.formProfile.get('email')?.value : this.authUser.email,
      password: this.formProfile.get('password')?.value ? this.formProfile.get('password')?.value : this.authUser.password,
      rol: this.authUser.rol 
    }

    if (this.formProfile.valid){
      this.userService.update(this.authUser._id, UserUpdate).subscribe(
        data => {
          if (data.exist) {
            this.messagesService.showMessage('Actualizar', false, 'Este correo electrónico ya esta asociado a otra cuenta.');
          } else if (data.success) {
            this.messagesService.showMessage('Actualizar', true, 'Actualizado con exito!');
            if (data.content.user) {
              localStorage.setItem('user_teleperformance', JSON.stringify(data.content.user));
              this.getUsers();
            }            
          }
        },
        () => {
          this.messagesService.showMessage('Actualizar', false, 'Error interno, intentelo más tarde.');
        }
      );      
    } else {
      this.messagesService.showMessage('Actualizar', false, 'Debes llenar todos los campos');
    }
  }

  getUsers(): void {
    this.userService.getUsers().subscribe(
      datos => {
        if (datos.success) {
          this.users = datos.content.users!;
          this.usersTable = this.generateUsersForTable(this.users);
          this.esVisible = true;
        }
      },
      () => {
        this.toastr.error('No se pueden cargar los personajes', 'Error en API');
      }      
    );    
  }

  generateUsersForTable(users: User[]): any {
    const filteredUsers = users.map(element => {
      delete element.createdAt;
      delete element.updatedAt;
      return element;      
    });
    return filteredUsers;
  }

  get name(): any { return this.formProfile.get('name'); }
  get email(): any { return this.formProfile.get('email'); }
  get password(): any { return this.formProfile.get('password'); }

}
