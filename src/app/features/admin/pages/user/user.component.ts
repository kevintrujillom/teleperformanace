import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CharacterCustom } from 'src/app/core/interfaces/characters/character-custom';
import { Character } from 'src/app/core/interfaces/characters/character.interface';
import { User } from 'src/app/core/interfaces/users/user-interface';
import { CharactersService } from 'src/app/core/services/characters/characters.services';
import { UserService } from 'src/app/core/services/users/user-service';
import { MessagesService } from 'src/app/core/services/utils/messages.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  formProfile!: FormGroup;
  emailPattern: any = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  
  authUser!: User;
  characters: Character[] = [];
  charactersTable: CharacterCustom[] = [];
  esVisible = false;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private messagesService: MessagesService,    
    private charactersService: CharactersService, 
    private toastr: ToastrService
  ) { 
    this.createForm();
  }
  
  ngOnInit(): void {
    this.getUser();
    this.getCharacters();
  }

  createForm(){
    this.formProfile = this.formBuilder.group({
      name: [''],
      email: ['', Validators.pattern(this.emailPattern)],
      password: ['']
    });
  }

  getUser(): void {
    this.authUser = JSON.parse(localStorage.getItem('user_teleperformance')!);
    this.formProfile.get('name')!.setValue(this.authUser.name);
    this.formProfile.get('email')!.setValue(this.authUser.email);
  }

  update(): void {
    const UserUpdate = {
      name: this.formProfile.get('name')?.value ? this.formProfile.get('name')?.value : this.authUser.name,
      email: this.formProfile.get('email')?.value ? this.formProfile.get('email')?.value : this.authUser.email,
      password: this.formProfile.get('password')?.value ? this.formProfile.get('password')?.value : this.authUser.password,
      rol: this.authUser.rol 
    }

    if (this.formProfile.valid){
      this.userService.update(this.authUser._id, UserUpdate).subscribe(
      data => {
          if (data.exist) {
            this.messagesService.showMessage('Actualizar', false, 'Este correo electrónico ya esta asociado a otra cuenta.');
          } else if (data.success) {
            this.messagesService.showMessage('Actualizar', true, 'Actualizado con exito!');
            if (data.content.user) {
              localStorage.setItem('user_teleperformance', JSON.stringify(data.content.user));
              this.getCharacters();
            }            
          }
        },
        () => {
          this.showMessage(false, 'Error interno, intentelo más tarde.');
        }
      );      
    } else {
      this.showMessage(false, 'Debes llenar todos los campos');
    }
  }

  getCharacters(): void {
    this.charactersService.getCharacters().subscribe(
      datos => {
        this.characters = datos.results!;
        this.charactersTable = this.generateCharactersForTable(datos.results!);
        this.esVisible = true;
      },
      () => {
        this.toastr.error('No se pueden cargar los personajes', 'Error en API');
      }      
    );    
  }

  generateCharactersForTable(characters: Character[]): any {
    const filteredCharacters = characters.map(element => {
      delete element.created;
      delete element.episode;
      delete element.image;
      delete element.location;
      delete element.origin;
      delete element.status;
      delete element.type;
      delete element.url;
      return element;      
    });
    return filteredCharacters;
  }

  showMessage(success: boolean, message: string = '') {
    if (success) {
      this.toastr.success(message, 'Registro');
    } else {
      this.toastr.error(message, 'Registro');
    }
  }

  get name(): any { return this.formProfile.get('name'); }
  get email(): any { return this.formProfile.get('email'); }
  get password(): any { return this.formProfile.get('password'); }  

}
