import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth.service';
import { MessagesService } from 'src/app/core/services/utils/messages.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  formLogin!: FormGroup;
  emailPattern: any = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

  constructor(
    private formBuilder: FormBuilder, 
    private authService: AuthService,
    private messagesService: MessagesService  
  ) { 
    this.createForm();
  }

  ngOnInit(): void {
    this.isLogin();
  }

  createForm(): void {
    this.formLogin = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
      password: ['', [Validators.required]]
    });
  } 

  isLogin(): void {
    const AuthUser = JSON.parse(localStorage.getItem('user_teleperformance')!);
    if (AuthUser) {
      this.authService.redirect(AuthUser);
    }
  }

  login(): void {
    if (this.formLogin.valid){
      this.authService.login(this.formLogin.value).subscribe(
        data  => {
          if (data.success) {
            localStorage.setItem('user_teleperformance', JSON.stringify(data.content.user));
            this.authService.loginObser(true);
            this.authService.redirect(data.content.user!);
          }
        },
        () => {
          localStorage.removeItem('user_teleperformance');
          this.messagesService.showMessage('Login', false, 'Datos de acceso incorrectos.');
        }
      );      
    } else {
      this.messagesService.showMessage('Login', false, 'Debes llenar todos los campos');
    }
  }

  get email(): any { return this.formLogin.get('email'); }
  get password(): any { return this.formLogin.get('password'); }    

}
