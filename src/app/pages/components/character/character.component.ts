import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Character } from 'src/app/core/interfaces/characters/character.interface';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.scss']
})
export class CharacterComponent implements OnInit {

  @Input() character!: Character;
  @Output() characterSended: EventEmitter<Character> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  emmitCharacter(){
    this.characterSended.emit(this.character);
  }

}
