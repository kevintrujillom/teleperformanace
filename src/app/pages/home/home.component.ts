import { Component, OnInit } from '@angular/core';
import { Character } from 'src/app/core/interfaces/characters/character.interface';
import { CharactersService } from 'src/app/core/services/characters/characters.services';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from './../../shared/dialog/dialog.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  actualPage: number = 1;
  characters: Character[] = [];
  esVisible = false;

  constructor(
    private charactersService: CharactersService,
    private toastr: ToastrService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(): void {
    this.charactersService.getCharacters().subscribe(
      datos => {
        this.characters = datos.results!;
      },
      () => {
        this.esVisible = true;
        this.toastr.error('No se pueden cargar los personajes', 'Error en API');
      }      
    );
  }

  sayHello(character: Character): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {name: character.name}
    });

    dialogRef.afterClosed();
  }

}
