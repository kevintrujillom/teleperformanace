import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth.service';
import { UserService } from 'src/app/core/services/users/user-service';
import { ToastrService } from 'ngx-toastr';
import { Rol } from 'src/app/core/interfaces/users/rol-interface';
import { MessagesService } from 'src/app/core/services/utils/messages.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  formRegister!: FormGroup;
  emailPattern: any = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  listaRoles: Rol[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private userService: UserService,
    private messagesService: MessagesService, 
    private toastr: ToastrService
  ) {
    this.createForm();
   }

  ngOnInit(): void {
    this.isLogin();
    this.getRols();   
  }

  createForm(): void {
    this.formRegister = this.formBuilder.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
      password: ['', [Validators.required]],
      rol: ['', [Validators.required]],
    });
  }

  getRols(): void {
    this.userService.getRols().subscribe(
      data => {
        this.listaRoles = data;
      },
      () => {
        this.toastr.error('No se pueden cargar los roles', 'Error en API');
      }
    );
  }

  isLogin(): void {
    const AuthUser = JSON.parse(localStorage.getItem('user_teleperformance')!);
    if (AuthUser) {
      this.authService.redirect(AuthUser);
    }
  }

  register(): void {
    if (this.formRegister.valid){
      this.userService.register(this.formRegister.value).subscribe(
        data => {
          if (data.exist) {
            this.messagesService.showMessage('Registro', false, 'Este correo electrónico ya esta asociado a otra cuenta.');
          } else if (data.success) {
            localStorage.setItem('user_teleperformance', JSON.stringify(data.content.user));
            this.messagesService.showMessage('Registro', true, 'Bienvenido a Teleperformance!');
            this.authService.loginObser(true);         
            this.authService.redirect(data.content.user!);
          }
        },
        () => {
          localStorage.removeItem('user_teleperformance');
          this.messagesService.showMessage('Registro', false, 'Error interno, intentelo más tarde.');
        }
      );
    } else {
      this.messagesService.showMessage('Registro', false, 'Debes llenar todos los campos');
    }
  }
 
  get name(): any { return this.formRegister.get('name'); }
  get email(): any { return this.formRegister.get('email'); }
  get password(): any { return this.formRegister.get('password'); }
  get rol(): any { return this.formRegister.get('rol'); }    

}
