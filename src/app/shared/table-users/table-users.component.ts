import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { UserCustom } from 'src/app/core/interfaces/users/user-custom.interface';


@Component({
  selector: 'app-table-users',
  templateUrl: './table-users.component.html',
  styleUrls: ['./table-users.component.scss']
})
export class TableUsersComponent implements OnInit {

  @Input() users!: UserCustom[];
  
  displayedColumns: string[] = [ 'name', 'email', 'rol'];
  dataSource = new MatTableDataSource<UserCustom>(this.users);
  
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor() { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<UserCustom>(this.users);
  }

}
