import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { CharacterCustom } from 'src/app/core/interfaces/characters/character-custom';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @Input() characters!: CharacterCustom[];
  
  displayedColumns: string[] = ['id', 'name', 'gender', 'species'];
  dataSource = new MatTableDataSource<CharacterCustom>(this.characters);
  
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
    
  constructor() { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<CharacterCustom>(this.characters);
  }

}
