import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/core/interfaces/users/user-interface';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  authUser!: User;
  isVisible = false;

  constructor(
    private router: Router,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.getUser();
    this.isLogout();
    this.isLogin();   
  }

  getUser(): void {
    this.authUser = JSON.parse(localStorage.getItem('user_teleperformance')!);
    if (this.authUser) {
      this.isVisible = true;
    }
  }
  
  login(): void {
    this.router.navigate(['login']);
  }

  isLogin(): void {
    this.authService.loginObservable.subscribe(
      datos => {
        if (datos) {
          this.isVisible = true;
        }
      }
    );    
  }  

  profile(): void{
    this.authService.redirect(this.authUser);
  }

  logout(): void {
    localStorage.removeItem('user_teleperformance');
    this.authService.logout(true);
    this.router.navigate(['login']);
  }

  isLogout(): void {
    this.authService.logoutObservable.subscribe(
      datos => {
        if (datos) {
          this.isVisible = false;
        }
      }
    );    
  }

}
